/************************************************************************
* Programmer: Emma Mickas                                               *
* CptS 223, Spring 2020                                                 *
* Programming Assignment 2                                              *
* February 13, 2020                                                     *
* Decription: This file contains the Vector class declaration for the   *
*             STL vector containing objects of type Destination.        *
************************************************************************/

#pragma once
#include "Destination.h"
#include <vector>


class VectorMyJosephus : public std::vector<Destination>
{
private:
    int M; //Number of passes before elimination when eliminating destinations

    int N; //Number of destinations in the vector

public:
    VectorMyJosephus();

    VectorMyJosephus(int nM, int nN);

    ~VectorMyJosephus();

    void clear(); //Completely empties the vector list

    int currentSize(); //Returns the current size of the vector list

    bool isEmpty(); //Determines whether or not the vector list is empty

    Destination eliminateDestination(int &curPos, int position); //Eliminates a destination

    void printAllDestinations(); //Displays all destinations

    int getM(); //Standard getter for M

    int getN(); //Standard getter for N

    void setM(int nM); //Standard setter for M

    void setN(int nN); //Standard setter for N
};


void testVectorFixedM(std::ofstream &outFile); //Runs a test for a vector list with fixed M and varying N

void testVectorFixedN(std::ofstream &outFile); //Runs a test for a vector list with fixed N and varying M

void readDestinations(std::ifstream& fileStream, VectorMyJosephus& destinationVector); //Reads in a list of destinations from file to store in vector list

Destination runElimination(VectorMyJosephus& destinationVector); //Runs all rounds of eliminiations to find final destination

void reportVectorResults(Destination eliminationSequence[], Destination finalDestination, int size); //Reports final results of elimination

void writeVectorTimeStatistics();
