prog: main.o Destination.o ListMyJosephus.o VectorMyJosephus.o TestListMyJosephus.o TestVectorMyJosephus.o
	g++ main.o Destination.o ListMyJosephus.o VectorMyJosephus.o TestListMyJosephus.o TestVectorMyJosephus.o -o runProg

main.o: main.cpp Destination.h ListMyJosephus.h VectorMyJosephus.h
	g++ -c -g -Wall -std=c++11 main.cpp

Destination.o: Destination.cpp
	g++ -c -g -Wall -std=c++11 Destination.cpp

ListMyJosephus.o: ListMyJosephus.cpp ListMyJosephus.h
	g++ -c -g -Wall -std=c++11 ListMyJosephus.cpp

VectorMyJosephus.o: VectorMyJosephus.cpp VectorMyJosephus.h
	g++ -c -g -Wall -std=c++11 VectorMyJosephus.cpp

TestListMyJosephus.o: TestListMyJosephus.cpp ListMyJosephus.h
	g++ -c -g -Wall -std=c++11 TestListMyJosephus.cpp

TestVectorMyJosephus.o: TestVectorMyJosephus.cpp VectorMyJosephus.h
	g++ -c -g -Wall -std=c++11 TestVectorMyJosephus.cpp

clean:
	-rm *.o

run: @./runProg