# JosephusProblem

This project uses the Josephus problem to test the differences between the std C++ vector and list. Different values of m and n are tested to select a destination from a list of destinations, and the results of the program are written to results.log.