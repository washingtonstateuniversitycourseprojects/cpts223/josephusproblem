/************************************************************************
* Programmer: Emma Mickas                                               *
* CptS 223, Spring 2020                                                 *
* Programming Assignment 2                                              *
* February 13, 2020                                                     *
* Decription: This file contains the Destination class declaration      *
*             which is the object in the vector and list classes.       *
************************************************************************/

#pragma once
#include <iostream>
#include <string>


class Destination
{

private:

    int position; //Position of object in list/vector list

    std::string name; //Name of destination of object

public:

    Destination(); //Default constructor

    Destination(const Destination &nDestination); //Copy constructor

    Destination(int nPosition, std::string nName); //Constructor accepting values for private variables

    ~Destination(); //Destructor

    void printPosition(); //Displays position of object in list/vector list

    void printDestinationName(); //Displays name of destination in object

    int getPosition(); //Returns position of object in list/vector list

    std::string getName(); //Returns name of destination of object

    void setPosition(int nPosition); //Sets position of object in list/vector list

    void setName(std::string nName); //Sets name of destination of object

};



