/************************************************************************
* Programmer: Emma Mickas                                               *
* CptS 223, Spring 2020                                                 *
* Programming Assignment 2                                              *
* February 13, 2020                                                     *
* Decription: This file contains function definitions for the           *
*             VectorMyJosephus class.                                   *
************************************************************************/

#include "VectorMyJosephus.h"


/************************************************************************
* Programming Assignment 2                                              *
* Default constructor:                                                  *
*                                                                       *
* Description: Constructs an object for vector list                     *
************************************************************************/

VectorMyJosephus::VectorMyJosephus()
{
    M = 0; //Initialize M

    N = 0; //Initialize N
}


/************************************************************************
* Programming Assignment 2                                              *
* Default constructor:                                                  *
*                                                                       *
* Description: Constructs an object for vector list with set M and N    *
************************************************************************/

VectorMyJosephus::VectorMyJosephus(int nM, int nN)
{
    M = nM; //Set M to new M

    N = nN; //Set N to new N
}



/************************************************************************
* Programming Assignment 2                                              *
* Destructor:                                                           *
*                                                                       *
* Description: Destroys vector list                                     *
************************************************************************/

VectorMyJosephus::~VectorMyJosephus()
{
    clear(); //Destroy vector list
}


/************************************************************************
* Programming Assignment 2                                              *
* Clear:                                                                *
*                                                                       *
* Description: Increments through vector list destroying each object    *
************************************************************************/

void VectorMyJosephus::clear()
{
    while (!isEmpty()) //Loop until vector list is empty
    {
        erase(begin()); //Erase each object one by one
    }

}



/************************************************************************
* Programming Assignment 2                                              *
* CurrentSize:                                                          *
*                                                                       *
* Description: Returns the current number of objects in the list        *
************************************************************************/

int VectorMyJosephus::currentSize()
{
    int size = 0; //Initialize count variable

    std::vector<Destination>::iterator it = begin(); //Initialize iterator

    while (it != end()) //Loop until end of vector list
    {
        size++; //Increment size

        it++; //Increment iterator
    }

    return size; //Return final size
}



/************************************************************************
* Programming Assignment 2                                              *
* IsEmpty:                                                              *
*                                                                       *
* Description: Determines if the vector list has no objects             *
************************************************************************/

bool VectorMyJosephus::isEmpty()
{
    return empty(); //Check if vector is empty
}



/************************************************************************
* Programming Assignment 2                                              *
* Eliminate Destination:                                                *
*                                                                       *
* Description: Destroys vector list                                     *
************************************************************************/

Destination VectorMyJosephus::eliminateDestination(int &curPos, int position)
{
    std::vector<Destination>::iterator itEliminate = begin();

    for (int i = 0; i < position; i++) //Loop until position to delete has been reached
    {
        curPos++;        

        if ((curPos == N) && (i < position)) //If end of list is reached
        {

            curPos = 0; //Set iterator back to front of list

        }
    }

    Destination destination = (*this)[curPos]; //Set destination variable to return to destination about to be deleted

    for (int i = 0; i < curPos; i++)
    {
        itEliminate++; //Set object being deleted to separate variable as to not lose place in list
    }

    erase(itEliminate); //Delete destination to eliminate

    N--; //Update N

    if (curPos == N) //If incrementing iterator leads to end of list
    {
        curPos = 0; //Reset iterator to front of list
    }

    return destination; //Return eliminated destination
}



/************************************************************************
* Programming Assignment 2                                              *
* Print All Destinations:                                               *
*                                                                       *
* Description: Displays all destinations curently in vector list        *
************************************************************************/

void VectorMyJosephus::printAllDestinations()
{
    std::vector<Destination>::iterator it = begin(); //Set iterator to front of vector list

    std::cout << "{"; //Display list surrounded by brackets

    while (it != end()) //Loop until end of list
    {
        std::cout << "\"" << (*it).getName(); //Display each destination surrounded by quotes

        it++; //Incrment iterator

        if (it != end()) //If end of vector list has not been reached
        {
            std::cout << "\", "; //Print comma to separate destinations
        }
    }

    std::cout << "\"}" << std::endl; //Display list surrounded by brackets
}



/************************************************************************
* Programming Assignment 2                                              *
* getM:                                                                 *
*                                                                       *
* Description: Returns value of M                                       *
************************************************************************/

int VectorMyJosephus::getM()
{
    return M;
}



/************************************************************************
* Programming Assignment 2                                              *
* getN:                                                                 *
*                                                                       *
* Description: Returns value of N                                       *
************************************************************************/

int VectorMyJosephus::getN()
{
    return N;
}



/************************************************************************
* Programming Assignment 2                                              *
* setM:                                                                 *
*                                                                       *
* Description: Sets new value of M                                      *
************************************************************************/

void VectorMyJosephus::setM(int nM)
{
    M = nM;
}



/************************************************************************
* Programming Assignment 2                                              *
* setN:                                                                 *
*                                                                       *
* Description: Sets new value of N                                      *
************************************************************************/

void VectorMyJosephus::setN(int nN)
{
    N = nN;
}
