/************************************************************************
* Programmer: Emma Mickas                                               *
* CptS 223, Spring 2020                                                 *
* Programming Assignment 2                                              *
* February 13, 2020                                                     *
* Decription: This file contains the test functions for the STL vector. *
************************************************************************/

#include "VectorMyJosephus.h"
#include <fstream>
#include <ctime>
#include <cstdlib>



/************************************************************************
* Programming Assignment 2                                              *
* Test vector Fixed M:                                                  *
*                                                                       *
* Description: Tests vector STL with fixed value of M                   *
************************************************************************/

void testVectorFixedM(std::ofstream &outFile)
{
    VectorMyJosephus destinationVector; //Initialize vector list of destinations

    destinationVector.setM(3); //Set fixed M to 3

    Destination finalDestination; //Initialize final destination

    std::ifstream inFile; //Initialize infile

    std::string throwaway;

    outFile << "\nVector time statistics, fixed M, varying N: \n" << std::endl;

    for (int i = 4; i <= 1024; i *= 2) //Loop for all test values of N
    {
        clock_t start = clock(); //Start timer

        destinationVector.setN(i); //Set N to test value

        inFile.open("destinations.csv"); //Open destinations.csv to read in destinations

        readDestinations(inFile, destinationVector); //Read in destinations to vector list

        inFile.close(); //Close infile after reading

        finalDestination = runElimination(destinationVector); //Eliminate all destinations but one to find final destination

        clock_t end = clock(); //End timer

        outFile << "CPU elapsed time in milliseconds with M fixed at 3, N = " << i << std::endl << ((double)(end - start) / CLOCKS_PER_SEC) * 1000 << std::endl; //Write total time statistics to outfile

        outFile << "Average elimination time: " << (((double)(end - start) / CLOCKS_PER_SEC) / i) * 1000 << "\n" << std::endl; //Write average time statistics to outfile

        destinationVector.clear(); //Clear destination list for next
    }

    outFile << std::endl;
}



/************************************************************************
* Programming Assignment 2                                              *
* Test Vector Fixed N:                                                  *
*                                                                       *
* Description: Tests STL vector list with fixed value of N              *
************************************************************************/

void testVectorFixedN(std::ofstream &outFile)
{
    VectorMyJosephus destinationVector; //Initialize destination vector list

    Destination finalDestination; //Initialize final destination

    std::ifstream inFile; //Initialize infile

    outFile << "Vector time statistics, fixed N, varying M: \n" << std::endl;

    for (int i = 2; i < 512; i *= 2) //Loop for all test values of M
    {
        clock_t start = clock(); //Start timer

        destinationVector.setM(i); //Set M to test value

        destinationVector.setN(512); //Set fixed N to 512

        inFile.open("destinations.csv"); //Open destinations.csv to read in destinations

        readDestinations(inFile, destinationVector); //Read in destinations to vector list

        inFile.close(); //Close infile after reading

        finalDestination = runElimination(destinationVector); //Eliminate all destinations but one to find final destination

        clock_t end = clock(); //End timer

        outFile << "CPU elapsed time in milliseconds with N fixed at 512, M = " << i << std::endl << ((double)(end - start) / CLOCKS_PER_SEC) * 1000 << std::endl; //Write total time statistics to outfile

        outFile << "Average elimination time: " << (((double)(end - start) / CLOCKS_PER_SEC) / i) * 1000 << "\n" << std::endl; //Write average time statistics to outfile

        destinationVector.clear(); //Clear destination list for next
    }
}



/************************************************************************
* Programming Assignment 2                                              *
* Read Destinations:                                                    *
*                                                                       *
* Description: Reads in destinations from a random line of infile and   *
*              stores them in destination vector list                   *
************************************************************************/

void readDestinations(std::ifstream& fileStream, VectorMyJosephus& destinationVector)
{

    int i = -1, k = 0; //Initialize variables to keep track of lines and destinations read in
    
    while ((i < 0) || (i > 25)) //Loops until valid integer is selected
    {
        i = rand() % 25; //Randomly select number of line to read in destinations from
    }

    std::string throwaway; //Initialize variable to hold unused lines

    for (int j = 0; j < i; j++) //Loop until line to read in destinations from
    {
        getline(fileStream, throwaway); //Increment through unused lines
    }

    std::string newName; //Initialize new name varaible

    Destination newDestination; //Intialize new destination variable

    while ((newName != "\n") && (k < destinationVector.getN())) //Loop until end of line or vector list is full
    {
        std::getline(fileStream, newName, '\"'); //Read in line until quote

        if (newName == "") //If name is empty after read in
        {
            std::getline(fileStream, newName, '\"'); //Read in line until quote again
        }

        newDestination.setName(newName); //Set name of destination object to new name

        newDestination.setPosition(k); //Set position of destination object to new position

        destinationVector.insert(destinationVector.end(), newDestination); //Insert new destination object into destination vector list

        k++; //Increment number of destinations that have been read in

        std::getline(fileStream, newName, '\"'); //Read in end quote to move on to next destination
    }

}



/************************************************************************
* Programming Assignment 2                                              *
* Run Elimination  :                                                    *
*                                                                       *
* Description: Eliminates destinations from vector list until only one  *
*              remains                                                  *
************************************************************************/

Destination runElimination(VectorMyJosephus& destinationVector)
{
    std::vector<Destination>::iterator it = destinationVector.begin(); //Initialize iterator to start of vector list

    int size = destinationVector.getN(); //Initialize size to N

    Destination eliminationSequence[1025]; //Initialize sequence of elimination

    Destination eliminatedDestination; //Initialize destination being eliminated in each round

    //std::cout << "Destinations in original vector list - ";

    //destinationVector.printAllDestinations(); //Display original vector list

    //std::cout << std::endl;

    int curPos = 0;

    for (int i = size; i > 1; i--) //Loop until all destinations have been eliminated but one
    {
        //std::cout << "Elimination round " << size - (i - 1) << ":" << std::endl; //Display elimination round

        eliminatedDestination = destinationVector.eliminateDestination(curPos, destinationVector.getM()); //Perform elimination

        //std::cout << "Destinations in vector list after elimination - ";

        //destinationVector.printAllDestinations(); //Display vector list after elimination

        //std::cout << std::endl;

        eliminationSequence[size - i] = eliminatedDestination; //Add eliminated destination to elimination vector list
    }

    //std::cout << std::endl;

    //reportVectorResults(eliminationSequence, *(destinationVector.begin()), size); //Show final results of elimination

    return *(destinationVector.begin()); //Return final destination
}



/************************************************************************
* Programming Assignment 2                                              *
* Report Vector results:                                                *
*                                                                       *
* Description: Reports elimination sequence and final destination       *
************************************************************************/

void reportVectorResults(Destination eliminationSequence[], Destination finalDestination, int size)
{
    std::cout << "Elimination Sequence: {"; //Display elimination sequence in brackets

    for (int i = 0; i + 1 < size; i++) //Loop until end of elimination sequence
    {
        std::cout << "\"" << eliminationSequence[i].getPosition() << ". " << eliminationSequence[i].getName() << "\""; //Display destination
        
        if (i + 2 < size) //If end of elimination sequence has not been reached
        {
            std::cout << ", "; //Separate destinations with comma
        }
    }
    std::cout << "}" << std::endl; //Display elimination sequence in brackets

    std::cout << std::endl;

    std::cout << "Final destination: " << finalDestination.getPosition() << ". " << finalDestination.getName() << "\n\n" << std::endl; //Display final destination
}

