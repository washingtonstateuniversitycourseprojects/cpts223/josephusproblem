/************************************************************************
* Programmer: Emma Mickas                                               *
* CptS 223, Spring 2020                                                 *
* Programming Assignment 2                                              *
* February 13, 2020                                                     *
* Decription: This file contains function definitions for the           *
*             Destination class.                                        *
************************************************************************/

#include "Destination.h"



/************************************************************************
* Programming Assignment 2                                              *
* Default constructor:                                                  *
*                                                                       *
* Description: Constructs an object of type Destination                 *
************************************************************************/

Destination::Destination()
{
    position = 0; //Initialize position

    name = ""; //Initialize position
}



/************************************************************************
* Programming Assignment 2                                              *
* Copy constructor:                                                     *
*                                                                       *
* Description: Constructs an object of type Destination from existing   *
*              Destination object                                       *
************************************************************************/

Destination::Destination(const Destination &nDestination)
{
    position = nDestination.position; //Set position to position of existing object

    name = nDestination.name; //Set name to name of existing object
}



/************************************************************************
* Programming Assignment 2                                              *
* Default constructor:                                                  *
*                                                                       *
* Description: Constructs an object of type Destination from existing   *
************************************************************************/

Destination::Destination(int nPosition, std::string nName)
{
    position = nPosition; //Set position to new position

    name = nName; //Set name to new name
}



/************************************************************************
* Programming Assignment 2                                              *
* Destructor:                                                           *
*                                                                       *
* Description: Destroys object of type destination                      *
************************************************************************/

Destination::~Destination()
{

}



/************************************************************************
* Programming Assignment 2                                              *
* Print Position:                                                       *
*                                                                       *
* Description: Prints position of object in list/vector list            *
************************************************************************/

void Destination::printPosition()
{
    std::cout << "The position of this destination is " << position << "." << std::endl; //Print position
}



/************************************************************************
* Programming Assignment 2                                              *
* Print Destination Name:                                               *
*                                                                       *
* Description: Displays destination name                                *
************************************************************************/

void Destination::printDestinationName()
{
    std::cout << "Destination: " << name;
}



/************************************************************************
* Programming Assignment 2                                              *
* getPosition:                                                          *
*                                                                       *
* Description: Returns position of object in list/vector list           *
************************************************************************/

int Destination::getPosition()
{
    return position;
}



/************************************************************************
* Programming Assignment 2                                              *
* getName:                                                              *
*                                                                       *
* Description: Returns name of object in list/vector list               *
************************************************************************/

std::string Destination::getName()
{
    return name;
}



/************************************************************************
* Programming Assignment 2                                              *
* setPosition:                                                          *
*                                                                       *
* Description: Sets position of object                                  *
************************************************************************/

void Destination::setPosition(int nPosition)
{
    position = nPosition; //Set position to new position
}



/************************************************************************
* Programming Assignment 2                                              *
* setName:                                                              *
*                                                                       *
* Description: Sets name of object                                      *
************************************************************************/

void Destination::setName(std::string nName)
{
    name = nName; //Set name to new name
}



