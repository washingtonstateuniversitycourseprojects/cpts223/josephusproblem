/************************************************************************
* Programmer: Emma Mickas                                               *
* CptS 223, Spring 2020                                                 *
* Programming Assignment 2                                              *
* February 13, 2020                                                     *
* Decription: This file runs test functions on the two STL templates    *
*             Vector and List used in the project.                      *
* Questions:                                                            *
* 1. The processor of the machine that is running the algorithms does   *
* matter. Some processors are faster or more efficient at handling      *
* memory. As such, you will not recieve the same timing statistics on   *
* every machine.                                                        *
* 2. When M is fixed and N is varied, the vector structure is more      *
* efficient but only until N=128. If N is greater than 128, list is more*
* efficent.                                                             *
* When N is fixed and M is varied, list is always more efficient. A     *
* larger number of objects means vector needs to shift more items in the*
* case of an elimination, however in this case, the vector times        *
* vary less.                                                            *
* 3. In the list container, varying M creates a smaller range in time   *
* taken to eliminate a destination then varying M.                      *
* In the vector container, varying N creates a MUCH greater change in   *
* time to eliminate a destination. Varying N doesn't change time        *
* statistics much at all, likely due to the ability to reference a spot *
* in the vector directly.                                               *
************************************************************************/


//Time between consecutive eliminations as M changes


#include "ListMyJosephus.h"
#include "VectorMyJosephus.h"
#include <fstream>



int main(int argc, char* argv[])
{
    srand(time(NULL));

    std::ofstream outFile;

    outFile.open("results.log");

    testListFixedM(outFile);

    testListFixedN(outFile);

    testVectorFixedM(outFile);

    testVectorFixedN(outFile);

    outFile.close();
}


