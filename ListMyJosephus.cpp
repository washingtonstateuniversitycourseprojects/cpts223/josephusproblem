/************************************************************************
* Programmer: Emma Mickas                                               *
* CptS 223, Spring 2020                                                 *
* Programming Assignment 2                                              *
* February 13, 2020                                                     *
* Decription: This file contains function definitions for the           *
*             ListMyJosephus class.                                     *
************************************************************************/

#include "ListMyJosephus.h"


/************************************************************************
* Programming Assignment 2                                              *
* Default constructor:                                                  *
*                                                                       *
* Description: Constructs an object for list                            *
************************************************************************/

ListMyJosephus::ListMyJosephus()
{
    M = 0; //Initialize M

    N = 0; //Initialize N
}


/************************************************************************
* Programming Assignment 2                                              *
* Default constructor:                                                  *
*                                                                       *
* Description: Constructs an object for list with set M and N           *
************************************************************************/

ListMyJosephus::ListMyJosephus(int nM, int nN)
{
    M = nM; //Set M to new M

    N = nN; //Set N to new N
}



/************************************************************************
* Programming Assignment 2                                              *
* Destructor:                                                           *
*                                                                       *
* Description: Destroys list                                            *
************************************************************************/

ListMyJosephus::~ListMyJosephus()
{
    clear(); //Destroy list
}


/************************************************************************
* Programming Assignment 2                                              *
* Clear:                                                                *
*                                                                       *
* Description: Increments through list destroying each object           *
************************************************************************/

void ListMyJosephus::clear()
{
    while (!isEmpty()) //Loop until list is empty
    {
        erase(begin()); //Erase each object one by one
    }

}



/************************************************************************
* Programming Assignment 2                                              *
* CurrentSize:                                                          *
*                                                                       *
* Description: Returns the current number of objects in the list        *
************************************************************************/

int ListMyJosephus::currentSize()
{
    int size = 0; //Initialize count variable

    std::list<Destination>::iterator it = begin(); //Initialize iterator

    while (it != end()) //Loop until end of list
    {
        size++; //Increment size

        it++; //Increment iterator
    }

    return size; //Return final size
}



/************************************************************************
* Programming Assignment 2                                              *
* IsEmpty:                                                              *
*                                                                       *
* Description: Determines if the list has no objects                    *
************************************************************************/

bool ListMyJosephus::isEmpty()
{
    return empty(); //Check if list is empty
}



/************************************************************************
* Programming Assignment 2                                              *
* Eliminate Destination:                                                *
*                                                                       *
* Description: Destroys list                                            *
************************************************************************/

Destination ListMyJosephus::eliminateDestination(std::list<Destination>::iterator &it, int position)
{
    std::list<Destination>::iterator itEliminate; //Initialize variable to hold object to delete

    for (int i = 0; i < position; i++) //Loop until position to delete has been reached
    {

        if ((++it == end()) && (i < position)) //If end of list is reached
        {

            it = begin(); //Set iterator back to front of list

        }
    }

    Destination destination = *it; //Set destination variable to return to destination about to be deleted

    itEliminate = it; //Set object being deleted to separate variable as to not lose place in list

    if (++it == end()) //If incrementing iterator leads to end of list
    {
        it = begin(); //Reset iterator to front of list
    }

    erase(itEliminate); //Delete destination to eliminate

    N--; //Update size

    return destination; //Return eliminated destination
}



/************************************************************************
* Programming Assignment 2                                              *
* Print All Destinations:                                               *
*                                                                       *
* Description: Displays all destinations curently in list               *
************************************************************************/

void ListMyJosephus::printAllDestinations()
{
    std::list<Destination>::iterator it = begin(); //Set iterator to front of list

    std::cout << "{"; //Display list surrounded by brackets

    while (it != end()) //Loop until end of list
    {
        std::cout << "\"" << (*it).getName(); //Display each destination surrounded by quotes

        it++; //Incrment iterator

        if (it != end()) //If end of list has not been reached
        {
            std::cout << "\", "; //Print comma to separate destinations
        }
    }

    std::cout << "\"}" << std::endl; //Display list surrounded by brackets
}



/************************************************************************
* Programming Assignment 2                                              *
* getM:                                                                 *
*                                                                       *
* Description: Returns value of M                                       *
************************************************************************/

int ListMyJosephus::getM()
{
    return M;
}



/************************************************************************
* Programming Assignment 2                                              *
* getN:                                                                 *
*                                                                       *
* Description: Returns value of N                                       *
************************************************************************/

int ListMyJosephus::getN()
{
    return N;
}



/************************************************************************
* Programming Assignment 2                                              *
* setM:                                                                 *
*                                                                       *
* Description: Sets new value of M                                      *
************************************************************************/

void ListMyJosephus::setM(int nM)
{
    M = nM;
}



/************************************************************************
* Programming Assignment 2                                              *
* setN:                                                                 *
*                                                                       *
* Description: Sets new value of N                                      *
************************************************************************/

void ListMyJosephus::setN(int nN)
{
    N = nN;
}
