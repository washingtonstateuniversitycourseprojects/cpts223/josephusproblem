/************************************************************************
* Programmer: Emma Mickas                                               *
* CptS 223, Spring 2020                                                 *
* Programming Assignment 2                                              *
* February 13, 2020                                                     *
* Decription: This file contains the List class declaration for the     *
*             STL list containing objects of type Destination.          *
************************************************************************/

#pragma once
#include "Destination.h"
#include <list>


class ListMyJosephus : public std::list<Destination>
{
private:
    int M; //Number of passes before elimination when eliminating destinations

    int N; //Number of destinations in the list

public:
    ListMyJosephus();

    ListMyJosephus(int nM, int nN);

    ~ListMyJosephus();

    void clear(); //Completely empties the list

    int currentSize(); //Returns the current size of the list

    bool isEmpty(); //Determines whether or not the list is empty

    Destination eliminateDestination(std::list<Destination>::iterator &it, int position); //Eliminates a destination

    void printAllDestinations(); //Displays all destinations

    int getM(); //Standard getter for M

    int getN(); //Standard getter for N

    void setM(int nM); //Standard setter for M

    void setN(int nN); //Standard setter for N
};


void testListFixedM(std::ofstream &outFile); //Runs a test for a list with fixed M and varying N

void testListFixedN(std::ofstream &outFile); //Runs a test for a list with fixed N and varying M

void readDestinations(std::ifstream& fileStream, ListMyJosephus& destinationList); //Reads in a list of destinations from file to store in list

Destination runElimination(ListMyJosephus& destinationList); //Runs all rounds of eliminiations to find final destination

void reportListResults(Destination eliminationSequence[], Destination finalDestination, int size); //Reports final results of elimination

void writeListTimeStatistics();